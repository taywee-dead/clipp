#include "table.hxx"

#include <string>
#include <vector>
#include <list>
#include <sstream>
#include <iterator>

namespace Table
{
    std::string Row(const std::vector<std::string> &text, const std::vector<unsigned int> &lengths, const std::string &separator, const std::string &beginning, const std::string &ending)
    {
        std::vector<std::list<std::string>> words(text.size());
        for (unsigned int i = 0; i < text.size(); ++i)
        {
            std::istringstream wordstream(text[i]);
            std::copy(std::istream_iterator<std::string>(wordstream), std::istream_iterator<std::string>(), std::back_inserter(words[i]));
        }
        std::ostringstream output;

        while (true)
        {
            output << beginning;

            for (unsigned int i = 0; i < words.size(); ++i)
            {
                if (words[i].empty())
                {
                    output << std::string(lengths[i], ' ');
                } else
                {
                    output << words[i].front(); 

                    size_t space_left = lengths[i] - words[i].front().length();
                    words[i].pop_front();

                    while (!words[i].empty() && (words[i].front().length() + 1 <= space_left))
                    {
                        output << ' ' << words[i].front();
                        space_left -= (words[i].front().length() + 1);
                        words[i].pop_front();
                    }

                    output << std::string(space_left, ' ');
                }

                if (i != words.size() - 1)
                {
                    output << separator;
                } else
                {
                    output << ending;
                }
            }

            bool exit = true;
            for (unsigned int i = 0; i < words.size(); ++i)
            {
                if (!words[i].empty())
                {
                    output << '\n';
                    exit = false;
                    break;
                }
            }

            if (exit)
            {
                break;
            }
        }
        return output.str();
    }
}
