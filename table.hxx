#pragma once

#include <string>
#include <vector>

namespace Table
{
    extern std::string Row(const std::vector<std::string> &text, const std::vector<unsigned int> &lengths = std::vector<unsigned int>(), const std::string &separator = "|", const std::string &beginning = "|", const std::string &ending = "|");
}
