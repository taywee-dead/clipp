build = Release
objects = table.o
CC = clang
CXX = clang++
AR = ar

commonOptsAll = -Wall -Wextra -std=c++11 -fPIC
commonDebugOpts = -ggdb -O0 -DDEBUG
commonReleaseOpts = -O3 -march=native
commonOpts = $(commonOptsAll) $(common$(build)Opts)

compileOptsAll = -c
compileOptsRelease =
compileOptsDebug =
compileOpts = $(commonOpts) $(compileOptsAll) $(compileOpts$(build))

compile = $(CXX) $(compileOpts)

.PHONY : all clean

all : libclipp.a 

test : test.o libclipp.a
	$(CXX) -o $@ $^

clean :
	-rm -v libclipp.a $(objects) test.o test

libclipp.a : $(objects)
	$(AR) rcs $@ $^

table.o : table.cxx table.hxx
	$(compile) -o $@ table.cxx

test.o : test.cxx table.hxx
	$(compile) -o $@ test.cxx
