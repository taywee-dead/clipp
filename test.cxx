#include <iostream>
#include <string>
#include <vector>

#include "table.hxx"

int main()
{
    std::string text("In olden times when wishing still helped one, there lived a king "
            "whose daughters were all beautiful, but the youngest was so beautiful "
            "that the sun itself, which has seen so much, was astonished whenever "
            "it shone in her face.  Close by the king's castle lay a great dark "
            "forest, and under an old lime tree in the forest was a well, and when "
            "the day was very warm, the king's child went out into the forest and "
            "sat down by the side of the cool fountain, and when she was bored she "
            "took a golden ball, and threw it up on high and caught it, and this "
            "ball was her favorite plaything.");
    std::vector<std::string> texts;
    texts.push_back(text);
    texts.push_back(text);
    texts.push_back(text);
    std::vector<unsigned int> lengths;
    lengths.push_back(30);
    lengths.push_back(20);
    lengths.push_back(40);

    std::cout << "Wrapped at 30-20-40, spacing of 1:\n" << Table::Row(texts, lengths, "|", "", "") << std::endl;
    return 0;
}
